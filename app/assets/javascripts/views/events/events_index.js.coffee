class EatEatEat.Views.EventsIndex extends Backbone.View
  template: JST['events/index']
  initialize: ->
    for key, value of @collection
      @collection[key].on('reset', @render, this)

  render: ->
    @$el.html( @template())

    @collection.events.slice(0, 10).forEach(@appendEvent)

    @noUser()
    this

  noUser:  ->
    @$('#login-user').html("<button class='login-btn btn btn-primary'>Facebookでログイン</button>")

  appendEvent: (entry) ->
    view = new EatEatEat.Views.Events(model: entry)
    @$('#event-list').append(view.render().el)
class EatEatEat.Views.MyEventsIndex extends EatEatEat.Views.EventsIndex
  render: ->
    @$el.html( @template())

    id = parseInt( @id )
    @collection.events.where(user_id: id).forEach(@appendEvent)

    @model = @collection.users.get(@id)
    @showUser(@model)
    this

  showUser: (user) ->
    if user
      @$('.login-user-name').html(user.get('name'))

class EatEatEat.Views.Events extends Backbone.View
  template: JST['events/eventlist']
  tagName: 'li'
  className: 'event-data'

  render: ->
    d = @model.get('schedule_for')
    console.log d
    console.log @model
    @$el.html(@template(event: @model))
    this

