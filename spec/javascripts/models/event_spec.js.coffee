#=require models/event
 
describe 'Tests for Event', ->
  it 'Can be created with default values for its attributes', ->
    event = new EatEatEat.Models.Event()
    expect(event.get('owner')).toBeNull()
    expect(event.get('title')).toBe('')
    dt = event.get('created_at')
    now = new Date
    expect(dt).toBeLessThan(now)
    expect(event.get('schedule_for')).toBeNull()
    expect(event.get('entrants').length).toBe(0)
    expect(event.get('comments').length).toBe(0)

  it 'Fires a custom event when the state changes.', ->
    spy = jasmine.createSpy('-change event callback-')
    event = new EatEatEat.Models.Event()
 
    event.on 'change', spy
 
    event.set text: 'Get oil change for car'
    expect(spy).toHaveBeenCalled()

  it 'Can contain custom validation rules, and will trigger an error event on failed validation', ->
    errorCallback = jasmine.createSpy('-error event callback-')
    event = new EatEatEat.Models.Event()
    event.on 'invalid', errorCallback
    event.set created_at: 'a non-date value', {validate: true}
    expect(errorCallback).toHaveBeenCalled()
    errorArgs = errorCallback.mostRecentCall.args
 
    expect(errorArgs).toBeDefined
    expect(errorArgs[0]).toBe(event)
    expect(errorArgs[1]).toBe('Event.created_at must be a Date object.')
